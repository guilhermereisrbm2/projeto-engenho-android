package br.com.eusoft.projetoengenho;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

public class CustomOnItemSelectedListener implements OnItemSelectedListener {

    private String tipoDeClienteSelecionado="";

    public String getTipoDeClienteSelecionado() {
        return tipoDeClienteSelecionado;
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
        tipoDeClienteSelecionado = parent.getItemAtPosition(pos).toString();
        /*Toast.makeText(parent.getContext(),
                "OnItemSelectedListener : " + tipoDeClienteSelecionado,
                Toast.LENGTH_SHORT).show();
                */
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

}