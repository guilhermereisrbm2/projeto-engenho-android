package br.com.eusoft.projetoengenho;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.eusoft.projetoengenho.pagseguro.AppUtil;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroAddress;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroAreaCode;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroBrazilianStates;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroBuyer;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroCheckout;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroFactory;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroItem;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroPayment;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroPhone;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroPreApproval;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroShipping;
import br.com.eusoft.projetoengenho.pagseguro.PagSeguroShippingType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoginActivity extends Activity {

    public final static String INTENTEXTRA_USERNAME = "username";

    //private vars
    private List<Carregamento> carregamentos;
    private EditText editTextUsername;
    private EditText editTextPassword;
    private String username;
    private String password;
    private CustomOnItemSelectedListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Spinner spinner1 = (Spinner) findViewById(R.id.spinner);
        listener = new CustomOnItemSelectedListener();
        spinner1.setOnItemSelectedListener(listener);


        carregamentos = new ArrayList<>();

        Button buttonPagSeguro = (Button) findViewById(R.id.buttonPagSeguro);
        buttonPagSeguro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // at this point you should check if the user has internet connection
                // before stating the pagseguro checkout process.(it will need internet connection)

                // simulating an user buying an iphone 6
                final PagSeguroFactory pagseguro = PagSeguroFactory.instance();
                List<PagSeguroItem> shoppingCart = new ArrayList<>();
                shoppingCart.add(pagseguro.item("123", "PlayStation", BigDecimal.valueOf(1.00), 1, 300));
                PagSeguroPhone buyerPhone = pagseguro.phone(PagSeguroAreaCode.DDD81, "998187427");
                PagSeguroBuyer buyer = pagseguro.buyer("Ricardo Ferreira", "14/02/1978", "15061112000", "test@email.com.br", buyerPhone);
                PagSeguroAddress buyerAddress = pagseguro.address("Av. Boa Viagem", "51", "Apt201", "Boa Viagem", "51030330", "Recife", PagSeguroBrazilianStates.PERNAMBUCO);
                PagSeguroShipping buyerShippingOption = pagseguro.shipping(PagSeguroShippingType.PAC, buyerAddress);
                PagSeguroCheckout checkout = pagseguro.checkout("Ref0001", shoppingCart, buyer, buyerShippingOption);
                // starting payment process
                //new PagSeguroPayment(LoginActivity.this).pay(checkout.buildCheckoutXml());

                PagSeguroPreApproval preApproval = new PagSeguroPreApproval();
                //Log.d("TESTE","XML = "+preApproval.buildPreApprovalRequestXml());
                new PagSeguroPayment(LoginActivity.this).pay(preApproval.buildPreApprovalRequestXml());
            }
        });


        Button buttonLogin = (Button) findViewById(R.id.button);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tipo = listener.getTipoDeClienteSelecionado();
                Log.d("TESTE", "ESCOLHEU " + tipo);
                editTextUsername = (EditText) findViewById(R.id.editTextUsername);
                editTextPassword = (EditText) findViewById(R.id.editTextPassword);
                username = editTextUsername.getText().toString();
                password = editTextPassword.getText().toString();
                //getCarregamentosFromDB(0);
                if (tipo.equalsIgnoreCase("Prestador")) {
                    loginToDB(1);
                } else if (tipo.equalsIgnoreCase("Fornecedor")) {
                    loginToDB(2);
                }

            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_CANCELED) {
            // se foi uma tentativa de pagamento
            if(requestCode==PagSeguroPayment.PAG_SEGURO_REQUEST_CODE){
                // exibir confirmação de cancelamento
                final String msg = getString(R.string.transaction_cancelled);
                AppUtil.showConfirmDialog(this, msg, null);
            }
        } else if (resultCode == RESULT_OK) {
            // se foi uma tentativa de pagamento
            if(requestCode==PagSeguroPayment.PAG_SEGURO_REQUEST_CODE){
                // exibir confirmação de sucesso
                final String msg = getString(R.string.transaction_succeded);
                AppUtil.showConfirmDialog(this, msg, null);
            }
        }
        else if(resultCode == PagSeguroPayment.PAG_SEGURO_REQUEST_CODE){
            switch (data.getIntExtra(PagSeguroPayment.PAG_SEGURO_EXTRA, 0)){
                case PagSeguroPayment.PAG_SEGURO_REQUEST_SUCCESS_CODE:{
                    final String msg =getString(R.string.transaction_succeded);
                    AppUtil.showConfirmDialog(this,msg,null);
                    break;
                }
                case PagSeguroPayment.PAG_SEGURO_REQUEST_FAILURE_CODE:{
                    final String msg = getString(R.string.transaction_error);
                    AppUtil.showConfirmDialog(this,msg,null);
                    break;
                }
                case PagSeguroPayment.PAG_SEGURO_REQUEST_CANCELLED_CODE:{
                    final String msg = getString(R.string.transaction_cancelled);
                    AppUtil.showConfirmDialog(this,msg,null);
                    break;
                }
            }
        }
    }

    private void loginToDB(int id){

        AsyncTask<Integer, Void, Void> asyncTask = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {


                String extras = "&username="+username+"&password="+password;
                String complete_url = "http://grrbmphpsampleapp-env.us-west-2.elasticbeanstalk.com/masterdb_webservice.php?id="+params[0]+extras;
                Log.d("TESTE","complete_url="+complete_url);
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(complete_url)
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    String string_resposta = response.body().string();
                    if (string_resposta.equalsIgnoreCase("No results")){
                        Log.d("TESTE","NAO HA USUARIO COM ESSE NOME !");
                    }else{
                        Log.d("TESTE","string_resposta = "+string_resposta);
                        JSONArray array = new JSONArray(string_resposta);
                        Usuario usuario = null;
                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.getJSONObject(i);


                            usuario = new Usuario(object.getString("nome"), object.getString("senha"));



                        }
                        Log.d("TESTE","Encontrado usuario ! nome = "+usuario.getUsername()+", senha = "+usuario.getPassword());

                        if (params[0] == 1){
                            iniciarActivityPrestador(usuario.getUsername());
                        } else if (params[0] == 2){
                            iniciarActivityFornecedor(usuario.getUsername());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                //adapter.notifyDataSetChanged();
                for(int i = 0; i < carregamentos.size();i++){
                    Carregamento temp = carregamentos.get(i);

                    Log.d("TESTE",  "nome_fornecedor = "+temp.getNome_fornecedor()+", "+
                                    "nome_prestador = "+temp.getNome_prestador()+", "+
                                    "id_maquina = "+temp.getId_maquina()+", "+
                                    "data_hora = "+temp.getData_hora()+", "
                    );

                }
            }
        };

        asyncTask.execute(id);

    }
    private void iniciarActivityPrestador(String username){
        Log.d("TESTE", "INICIANDO ACTIVITY PRESTADOR");
        Intent intent = new Intent(this, PrestadorActivity.class);
        intent.putExtra(INTENTEXTRA_USERNAME, username);
        startActivity(intent);
    }
    private void iniciarActivityFornecedor(String username){
        Log.d("TESTE", "INICIANDO ACTIVITY FORNECEDOR");
        Intent intent = new Intent(this, FornecedorActivity.class);
        intent.putExtra(INTENTEXTRA_USERNAME, username);
        startActivity(intent);
    }


}
