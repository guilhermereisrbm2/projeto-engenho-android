package br.com.eusoft.projetoengenho;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FornecedorActivity extends Activity {

    private String TAG = "FornecedorActivity";
    private String username;
    private String dia_do_carregamento;
    private List<Carregamento> carregamentosDoFornecedor;
    private TableLayout tableLayout;
    private EditText editText;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fornecedor);

        Button buttonTeste = (Button) findViewById(R.id.buttonTeste);
        buttonTeste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRandomRow();
            }
        });

        //PARTE DA DATA--------------------------------------------------------------
        myCalendar = Calendar.getInstance();

        editText = (EditText) findViewById(R.id.editTextDate);
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        editText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(FornecedorActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //FIM PARTE DA DATA----------------------------------------------------------

        carregamentosDoFornecedor = new ArrayList<>();

        tableLayout = (TableLayout) findViewById(R.id.tableLayout);

        Intent intent = getIntent();
        username = intent.getStringExtra(LoginActivity.INTENTEXTRA_USERNAME);

        TextView textViewNomeFornecedor = (TextView) findViewById(R.id.textViewNomeFornecedor);
        textViewNomeFornecedor.setText(username);

        Button button = (Button) findViewById(R.id.buttonSearchFornecedor);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dia_do_carregamento = formatDate(myCalendar);
                //Log.d(TAG,"dia = "+day+", mês = "+month+", ano = "+year);
                getCarregamentosFornecedor(3);
                ((TextView)findViewById(R.id.textViewData)).setText(dia_do_carregamento);
            }
        });



    }
    private void addRandomRow(){
        TableRow row = (TableRow) LayoutInflater.from(FornecedorActivity.this).inflate(R.layout.atrrib_row,null);
        ((TextView)row.findViewById(R.id.fornecedor)).setText("1212");
        ((TextView)row.findViewById(R.id.prestador)).setText("1212");
        ((TextView)row.findViewById(R.id.maquina)).setText("1212");
        ((TextView)row.findViewById(R.id.data_hora)).setText("1212");
        ((TextView)row.findViewById(R.id.toneladas)).setText("1212");
        tableLayout.addView(row);
        tableLayout.requestLayout();

    }
    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        editText.setText(sdf.format(myCalendar.getTime()));
    }
    public String formatDate(Calendar calendar) {
        //Date Format ("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String output = formatter.format(calendar.getTime());

        return output;
    }

    private void getCarregamentosFornecedor(int id) {

        AsyncTask<Integer, Void, Void> asyncTask = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... movieIds) {


                //Date Format ("yyyy-MM-dd HH:mm:ss");
                String extras = "&nome_fornecedor="+username+"&dia_do_carregamento="+dia_do_carregamento;
                String complete_url="http://grrbmphpsampleapp-env.us-west-2.elasticbeanstalk.com/masterdb_webservice.php?id=" + movieIds[0] +extras;
                Log.d("TESTE","complete url = "+complete_url);
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(complete_url)
                        .build();
                try {
                    Response response = client.newCall(request).execute();
                    String string_resposta = response.body().string();
                    if (string_resposta.equalsIgnoreCase("No results")){
                        Log.d("TESTE","NAO HA RESULTADOS PARA ESSA BUSCA !");
                        FornecedorActivity.this.carregamentosDoFornecedor.clear();
                    } else {
                        Log.d("TESTE","RESULTADOS ENCONTRADOS !");
                        FornecedorActivity.this.carregamentosDoFornecedor.clear();
                        JSONArray array = new JSONArray(string_resposta);

                        for (int i = 0; i < array.length(); i++) {

                            JSONObject object = array.getJSONObject(i);


                            //then

                            Carregamento carregamento = new Carregamento(object.getString("nome_fornecedor"), object.getString("nome_prestador"),
                                    object.getInt("id_maquina"), object.getString("data_hora"), object.getInt("toneladas"));

                            FornecedorActivity.this.carregamentosDoFornecedor.add(carregamento);
                        }
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                //adapter.notifyDataSetChanged();
                tableLayout.removeViews(2,tableLayout.getChildCount()-2);
                for(int i = 0; i < carregamentosDoFornecedor.size();i++){
                    Carregamento temp = carregamentosDoFornecedor.get(i);
                    TableRow row = (TableRow) LayoutInflater.from(FornecedorActivity.this).inflate(R.layout.atrrib_row,null);
                    ((TextView)row.findViewById(R.id.fornecedor)).setText(temp.getNome_fornecedor()+"");
                    ((TextView)row.findViewById(R.id.prestador)).setText(temp.getNome_prestador() + "");
                    ((TextView)row.findViewById(R.id.maquina)).setText(temp.getId_maquina()+"");
                    ((TextView)row.findViewById(R.id.data_hora)).setText(temp.getData_hora()+"");
                    ((TextView)row.findViewById(R.id.toneladas)).setText(temp.getToneladas()+"");
                    tableLayout.addView(row);

                    Log.d("TESTE",  "id_fornecedor = "+temp.getNome_fornecedor()+", "+
                                    "id_prestador = "+temp.getNome_prestador()+", "+
                                    "id_maquina = "+temp.getId_maquina()+", "+
                                    "data_hora = "+temp.getData_hora()+", "+
                                    "toneladas = "+temp.getToneladas()+", "
                    );
                }
                tableLayout.requestLayout();
            }
        };

        asyncTask.execute(id);
    }

}
