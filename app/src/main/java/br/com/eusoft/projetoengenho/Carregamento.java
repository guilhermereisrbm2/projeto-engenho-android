package br.com.eusoft.projetoengenho;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Guilherme on 5/28/2017.
 */
public class Carregamento {

    private String nome_fornecedor;
    private String nome_prestador;
    private int id_maquina;
    private Date data_hora;
    private int toneladas;

    public Carregamento(String nome_fornecedor, String nome_prestador, int id_maquina, String data_hora, int toneladas) {
        this.nome_fornecedor = nome_fornecedor;
        this.nome_prestador = nome_prestador;
        this.id_maquina = id_maquina;
        this.setData_hora(data_hora);
        this.toneladas = toneladas;
    }

    public String getNome_fornecedor() {
        return nome_fornecedor;
    }

    public void setNome_fornecedor(String nome_fornecedor) {
        this.nome_fornecedor = nome_fornecedor;
    }

    public String getNome_prestador() {
        return nome_prestador;
    }

    public void setNome_prestador(String nome_prestador) {
        this.nome_prestador = nome_prestador;
    }

    public int getId_maquina() {
        return id_maquina;
    }

    public void setId_maquina(int id_maquina) {
        this.id_maquina = id_maquina;
    }

    public String getData_hora() {
        return this.data_hora.toString();
    }

    public void setData_hora(String data_hora) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = sdf.parse(data_hora);
            this.data_hora = date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public int getToneladas() {
        return toneladas;
    }

    public void setToneladas(int toneladas) {
        this.toneladas = toneladas;
    }
}
