package br.com.eusoft.projetoengenho.pagseguro;

/**
 * Created by Guilherme on 6/11/2017.
 */
public class PagSeguroPreApproval {


    public PagSeguroPreApproval(){

    }









    /** returns the pagseguro xml to execute a checkout request over the pagseguro webservice*/
    public String buildPreApprovalRequestXml(){

        String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>\n" +
                "<preApprovalRequest>\n" +
                "<preApproval>\n" +
                "       <reference>REF06508109456</reference>\n" +
                "       <name>Nome do Plano</name>\n" +     // Nome do plano
                "       <charge>MANUAL</charge>\n " + //Tipo de Cobrança
                "       <period>MONTHLY</period>\n" +  //Periodicidade do plano
                "       <cancelURL>http://sitedocliente.com</cancelURL>\n" +  //URL de cancelamento
                "       <amountPerPayment>1.00</amountPerPayment>\n" +  //Valor máximo cobrado por período
                "       <membershipFee>0.50</membershipFee>\n" +  //Taxa de adesão
                "       <trialPeriodDuration>28</trialPeriodDuration>\n" +  //Tempo de teste
                "       <expiration>\n" +
                "           <value>10</value>\n" + //Número de cobranças que serão realizadas
                "           <unit>months</unit>\n" +  //Período em que as cobranças serão realizadas
                "       </expiration>\n" +
                "</preApproval>\n" +
                "</preApprovalRequest>";
        return xml;
    }

}
